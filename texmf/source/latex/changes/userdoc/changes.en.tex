%^^A ---- introduction
\section{Introduction}

This package provides means for manual change markup.

Any comments, thoughts or improvements are welcome.
The package is maintained at \emph{gitlab}, please see

\url{https://edgesoft.de/projects/changes/}

for links to source code access, bug and feature tracker, etc.
If you want to contact me directly, please send an email to \href{mailto:ekleinod@edgesoft.de}{ekleinod@edgesoft.de}.
Please start your email subject with \texttt{[changes]}.

\begin{quote}
	The changes-package allows the user to manually markup changes of text, such as additions, deletions, or replacements.
	Changed text is shown in a different color; deleted text is striked out.
	Additionally, text can be highlighted and/or commented.
	The package allows free definition of additional authors and their associated color.
	It also allows you to change the markup of changes, authors, highlights or comments.
\end{quote}

Here is a short example of change markup:

\begin{quote}
	This is \added[id=EK, comment={missing word}]{new} text.
	In this sentence, I replace a \replaced[id=EK]{good}{bad} word.
	And, to sum up the text changes, there is another \deleted[id=EK]{obsolete} word to delete.
	Furthermore, text can be \highlight[id=EK]{highlighted} or just \comment[id=EK]{For the fun of it.}commented.
\end{quote}

Parallel to this manual is a folder ``examples'' which contains an extensive collection of example files, both \hologo{LaTeX} and PDF files.
Please refer to these examples for inspiration and first problem solving.


%^^A ---- usage
\cleardoublepage
\section{Using the \docpackage{changes}-package}
\label{sec:usage}

In this section a typical use case of the \docpackage{changes}-package is described.
You can find the detailed description of the package options and new commands in \autoref{sec:ui}.

We start with the text you want to change.
You want to markup the changes for each author individually.
Such a change markup is well-known in WYSIWYG text processors such as \emph{LibreOffice}, \emph{OpenOffice}, or \emph{Word}.

The \docpackage{changes}-package was developed in order to support such change markup.
The package provides commands for defining authors, and for marking text as added, deleted, or replaced.
Additionally, text can be highlighted or commented.
In order to use the package, you should follow these steps:

\begin{enumerate}
	\item use \docpackage{changes}-package
	\item define authors
	\item markup text changes
	\item highlight and comment text
	\item typeset the document with \hologo{LaTeX}
	\item output list of changes
	\item remove markup
\end{enumerate}


\minisec{Use \docpackage{changes}-package}

In order to activate change management, use the \docpackage{changes}-package as follows:

\chinline{usepackage_changes}

respectively

\chinline{usepackage_options_changes}

You can use the options for defining the layout of the change markup.
You can change the layout after using the \docpackage{changes}-package as well.

For detailed information please refer to \autoref{sec:ui:options} and \autoref{sec:ui:customizingoutput}.


\minisec{Define authors}

The \docpackage{changes}-package provides a default anonymous author.
If you want to track your changes depending on the author, you have to define the needed authors as follows:

\chinline{definechangesauthor}

Every author is uniquely identified through his or her id.
You can give every author an optional name and/or color.

For detailed information please refer to \autoref{sec:ui:authormanagement}.


\minisec{Markup text changes}

Now everything is set to markup the changed text.
Please use the following commands according to your change:

for added text:

\chinline{added}

for deleted text:

\chinline{deleted}

for replaced text:

\chinline{replaced}

Stating the author's id and/or a comment is optional.

For detailed information please refer to \autoref{sec:ui:changemanagement}.


\minisec{Highlight and comment text}

Maybe you want to highlight orcomment some text?

highlight text:

\chinline{highlight}

comment text:

\chinline{comment}

Stating the author's id and/or a comment for highlights is optional.

For detailed information please refer to \autoref{sec:ui:comment}.


\minisec{Typeset the document with \hologo{LaTeX}}

After marking your changes in the text you are able to display them in the generated document by processing it as usual with \hologo{LaTeX}.
By processing your document the changed text is layouted as you stated by the corresponding options and/or special commands.

\minisec{Output list of changes}

You can print a list of changes using:

\chinline{listofchanges}

The list is meant to be the analogon to the list of tables, or the list of figures.

Stating the style is optional, default is \docoption{style=list}.
In order to print a quick overview of the number and kind of changes of every author, use the option \docoption{style=summary} or  \docoption{style=compactsummary}.
Show only specific changes by using the \docoption{show} option.

By running \hologo{LaTeX} the data of the list is written into an auxiliary file.
This data is used in the next \hologo{LaTeX} run for typesetting the list of changes.
Therefore, two \hologo{LaTeX} runs are needed after every change in order to typeset an up-to-date list of changes.

For detailed information please refer to \autoref{sec:ui:overview}.


\minisec{Remove markup}

Often you want to remove the change markup after acknowledging or rejecting the changes.
You can suppress the output of changes with:

\chinline{usepackage_final_changes}

In order to remove the markup from the \hologo{LaTeX} files, you have to remove the commands by hand or use the script by Yvon Cui.
You find the script \texttt{pyMergeChanges.py} in the directory:

\chinline[, language=bash]{path_script}

The script removes all markups either keeping or rejecting the change.
You can select or deselect markup from removal using the interactive mode by starting the script without options.

For detailed information please refer to \autoref{sec:remove-markup}.




%^^A ---- limitations
\cleardoublepage
\section{Limitations and possible enhancements}
\label{sec:limitations}

The \docpackage{changes}-package was carefully programmed and tested.
Yet the possibility of errors in the package exists, you might encounter problem during use, or you might miss functionality.

You can find a list of th emost important known problems and possible solutions in \autoref{sec:known-problems}.
Please refer to the section first if your problem is known and is a solution exists.
More errors, problems, and solutions are provided at:

\url{https://edgesoft.de/projects/changes/}

or

\url{https://gitlab.com/ekleinod/changes/-/issues}

You can write me an email too, please send it to \href{mailto:ekleinod@edgesoft.de}{ekleinod@edgesoft.de}.
In that case, please start your email subject with \texttt{[changes]}.

Change markup of texts works well, it is possible to markup whole paragraphs.
You cannot markup:

\begin{itemize}
	\item figures
	\item tables
	\item headings
	\item some commands
	\item multiple paragraphs (sometimes)
\end{itemize}

You can try putting such text in an extra file and include in with \texttt{input}.
This works sometimes, give it a try.
Kudos to Charly Arenz for this tip.

If you experience errors about already defined macros, please see option \docoption{commandnameprefix}, \autoref{sec:ui:options:commandnameprefix}.

%^^A ---- user interface
\cleardoublepage
\section{User interface of the \docpackage{changes}-package}
\label{sec:ui}

This section describes the user interface of the \docpackage{changes}-package, i.e.\ all options and commands of the package.
Every option and new command is described.
If you want to see the options and commands in action, please refer to the examples in

\chinline[, language=bash]{path_doc_examples}

The example files are named with the used option respectively command.

%^^A -- options
\subsection{Package Options}
\label{sec:ui:options}

\chinline{usepackage_options_changes}

The package options control the behavior of the overall package, i.\,e.\ all markup commands.

The following options are defined:

\localtableofcontents



\subsubsection{draft}

\chinline{usepackage_draft_changes}

The \docoption{draft}-option enables markup of changes.
The list of changes is available via \doccommand{listofchanges}.
This option is the default option, if no other option is selected.

The \docpackage{changes} package reuses the declaration of \docoption{draft} in \doccommand{documentclass}.
The local declaration of \docoption{final} overrules the declaration of \docoption{draft} in \doccommand{documentclass}.

\subsubsection{final}

\chinline{usepackage_final_changes}

The \docoption{final}-option disables markup of changes, only the correct text will be shown.
The list of changes is disabled, too.

The \docpackage{changes} package reuses the declaration of \docoption{final} in \doccommand{documentclass}.
The local declaration of \docoption{draft} overrules the declaration of \docoption{final} in \doccommand{documentclass}.


\subsubsection{commandnameprefix}
\label{sec:ui:options:commandnameprefix}

\chinline{usepackage_commandnameprefix_changes}

The \docoption{commandnameprefix} option sets the prefixing strategy for the markup commands.
This is useful if another package already defined commands, e.g. \doccommand{comment} or \doccommand{highlight}.

Per default an error is raised if a command is already defined and no prefixing takes place (option not given or set to \docoption{none}).

If a prefix strategy is set, the command in question is prefixed with "ch".
The strategy determines which commands are prefixed.

This option only provides prefixed names for the markup commands:

\begin{itemize}
	\item \doccommand{added} \ensuremath{\to} \doccommand{chadded}
	\item \doccommand{deleted} \ensuremath{\to} \doccommand{chdeleted}
	\item \doccommand{replaced} \ensuremath{\to} \doccommand{chreplaced}
	\item \doccommand{highlight} \ensuremath{\to} \doccommand{chhighlight}
	\item \doccommand{comment} \ensuremath{\to} \doccommand{chcomment}
\end{itemize}

The following strategies for \emph{commandnameprefix} are provided:

\begin{description}
	\item [\docoption{none}] no prefix, a command already defined raises an error (default strategy)
	\item [\docoption{ifneeded}] if a command is already defined, \docpackage{changes} prefixes this command and raises a warning.
		Depending on the commands already defined, the document will contain a mix of prefixed and not prefixed markup commands.
		This is mostly used if only \doccommand{comment} or \doccommand{highlight} are already defined and you mainly want to use the change commands \doccommand{added}, \doccommand{deleted}, and \doccommand{replaced}.
	\item [\docoption{always}] all commands are prefixed, an according message is written to the log
\end{description}

\chexample{usepackage_commandnameprefix_changes}


\subsubsection{markup}

\chinline{usepackage_markup_changes}

The \docoption{markup} option chooses a predefined visual markup of changed text.
The default markup is chosen if no explicit markup is given.
The markup chosen with \docoption{markup} can be overwritten with the more special markup options \docoption{addedmarkup}, \docoption{deletedmarkup}, \docoption{commentmarkup}, or \docoption{highlightmarkup}.

The following values for \emph{markup} are defined:

\begin{description}
	\item [\docoption{default}] default markup for added and deleted text, comments and highlighted text (default markup)
	\item [\docoption{underlined}] underlined for added text, wavy underlined for highlighted text, default for deleted text, and comments
	\item [\docoption{bfit}] bold added text, italic deleted text, default for comments and highlighted text
	\item [\docoption{nocolor}] no colored markup, underlined for added text, wavy underlined for highlighted text, default for deleted text and comments
\end{description}

\chexample{usepackage_markup_changes}

When changing from color markup to markup without color and vice versa, some errors occur if an auxiliary file exists.
Please ignore the errors, they vanish in the second run.

\subsubsection{addedmarkup}

\chinline{usepackage_addedmarkup_changes}

The \docoption{addedmarkup} option chooses a predefined visual markup of added text.
The default markup is chosen if no explicit markup is given.
The option \docoption{addedmarkup} overwrites the markup chosen with \docoption{markup}.

The following values for \emph{addedmarkup} are defined:

\begin{description}
	\item [\docoption{colored}] no text markup, just coloring -- {\color{orange} example} (default)
	\item [\docoption{uline}] underlined text -- \uline{example}
	\item [\docoption{uuline}] double underlined text -- \uuline{example}
	\item [\docoption{uwave}] wavy underlined text -- \uwave{example}
	\item [\docoption{dashuline}] dashed underlined text -- \dashuline{example}
	\item [\docoption{dotuline}] dotted underlined text -- \dotuline{example}
	\item [\docoption{bf}] bold text -- \textbf{example}
	\item [\docoption{it}] italic text -- \textit{example}
	\item [\docoption{sl}] slanted text -- \textsl{example}
	\item [\docoption{em}] emphasized text -- \emph{example}
\end{description}

The output of replaced text is a combination of added and deleted text, thus any change in their layout influences the layout of replaced text.

\chexample{usepackage_addedmarkup_changes}


\subsubsection{deletedmarkup}
\label{sec:ui:options:deletedmarkup}

\chinline{usepackage_deletedmarkup_changes}

The \docoption{deletedmarkup} option chooses a predefined visual markup of deleted text.
The default markup is chosen if no explicit markup is given.
The option \docoption{deletedmarkup} overwrites the markup chosen with \docoption{markup}.

The following values for \emph{deletedmarkup} are defined:

\begin{description}
	\item [\docoption{sout}] striked out text -- \sout{example} (default)
	\item [\docoption{xout}] crossed out text -- \xout{example}
	\item [\docoption{colored}] no text markup, just coloring -- {\color{orange} example}
	\item [\docoption{uline}] underlined text -- \uline{example}
	\item [\docoption{uuline}] double underlined text -- \uuline{example}
	\item [\docoption{uwave}] wavy underlined text -- \uwave{example}
	\item [\docoption{dashuline}] dashed underlined text -- \dashuline{example}
	\item [\docoption{dotuline}] dotted underlined text -- \dotuline{example}
	\item [\docoption{bf}] bold text -- \textbf{example}
	\item [\docoption{it}] italic text -- \textit{example}
	\item [\docoption{sl}] slanted text -- \textsl{example}
	\item [\docoption{em}] emphasized text -- \emph{example}
\end{description}

The output of replaced text is a combination of added and deleted text, thus any change in their layout influences the layout of replaced text.

\chexample{usepackage_deletedmarkup_changes}


\subsubsection{highlightmarkup}

\chinline{usepackage_highlightmarkup_changes}

The \docoption{highlightmarkup} option chooses a predefined visual markup for highlighted text.
The default markup is chosen if no explicit markup is given.
The option \docoption{highlightmarkup} overwrites the markup chosen with \docoption{markup}.

The following values for \emph{highlightmarkup} are defined:

\begin{description}
	\item [\docoption{background}] markup by background color -- \colorbox{orange!30}{example} (default)
	\item [\docoption{uuline}] double underlined text -- \uuline{example}
	\item [\docoption{uwave}] wavy underlined text -- \uwave{example}
\end{description}

\chexample{usepackage_highlightmarkup_changes}



\subsubsection{commentmarkup}

\chinline{usepackage_commentmarkup_changes}

The \docoption{commentmarkup} option chooses a predefined visual markup for comments.
The default markup is chosen if no explicit markup is given.
The option \docoption{commentmarkup} overwrites the markup chosen with \docoption{markup}.

The following values for \emph{commentmarkup} are defined:

\begin{description}
	\item [\docoption{todo}] comment as todo note, which is not added to list of todos \todo{example comment}(default)
	\item [\docoption{margin}] comment in margin\marginpar{example comment}
	\item [\docoption{footnote}] comment as footnote\footnote{example comment}
	\item [\docoption{uwave}] wavy underlined text -- \uwave{example comment}
\end{description}

\chexample{usepackage_commentmarkup_changes}


\subsubsection{authormarkup}

\chinline{usepackage_authormarkup_changes}

The \docoption{authormarkup} option chooses a predefined visual markup of the author's identification.
The default markup is chosen if no explicit markup is given.

The following values for \emph{authormarkup} are defined:

\begin{description}
	\item [\docoption{superscript}] superscripted text -- text\textsuperscript{author} (default)
	\item [\docoption{subscript}] subscripted text -- text\textsubscript{author}
	\item [\docoption{brackets}] text in brackets -- text(author)
	\item [\docoption{footnote}] text in footnote -- text\footnote{author}
	\item [\docoption{none}] no author identification
\end{description}

\chexample{usepackage_authormarkup_changes}


\subsubsection{authormarkupposition}

\chinline{usepackage_authormarkupposition_changes}

The \docoption{authormarkupposition} option chooses the position of the author's identification.
The default value is chosen if no explicit markup is given.

The following values for \emph{authormarkupposition} are defined:

\begin{description}
	\item [\docoption{right}] right of the text -- text\textsuperscript{author} (default)
	\item [\docoption{left}] left of the text -- \textsuperscript{author}text
\end{description}

\chexample{usepackage_authormarkupposition_changes}



\subsubsection{authormarkuptext}

\chinline{usepackage_authormarkuptext_changes}

The \docoption{authormarkuptext} option chooses the text that is used for the author's identification.
The default value is chosen if no explicit markup is given.

The following values for \emph{authormarkuptext} are defined:

\begin{description}
	\item [\docoption{id}] author's id -- text\textsuperscript{id} (default)
	\item [\docoption{name}] author's name -- text\textsuperscript{authorname}
\end{description}

\chexample{usepackage_authormarkuptext_changes}



\subsubsection{defaultcolor}

\chinline{usepackage_defaultcolor_changes}

The \docoption{defaultcolor} option defines the default color for authors, including the color for the default (anonymous) author.
You can use colors of the \docpackage{xcolor} package.

The default color is \emph{blue}.

\chexample{usepackage_defaultcolor_changes}


\subsubsection{todonotes}

\chinline{usepackage_todonotes_changes}

Options for the \docpackage{todonotes} package can be specified as parameters of the \docoption{todonotes}-option.
Several options or options with special characters have to be put in curly brackets.

\chexample{usepackage_todonotes_changes}



\subsubsection{truncate}

\chinline{usepackage_truncate_changes}

Options for the \docpackage{truncate} package can be specified as parameters of the \docoption{truncate}-option.
Several options or options with special characters have to be put in curly brackets.

\chexample{usepackage_truncate_changes}


\subsubsection{ulem}

\chinline{usepackage_ulem_changes}

Options for the \docpackage{ulem} package can be specified as parameters of the \docoption{ulem}-option.
Several options or options with special characters have to be put in curly brackets.

\chexample{usepackage_ulem_changes}


\subsubsection{xcolor}

\chinline{usepackage_xcolor_changes}

Options for the \docpackage{xcolor} package can be specified as parameters of the \docoption{xcolor}-option.
Several options or options with special characters have to be put in curly brackets.

\chexample{usepackage_xcolor_changes}



%^^A -- Change management ----------------------------------------------------------
\subsection{Change management}
\label{sec:ui:changemanagement}

\localtableofcontents

\chnewcmd{added}

\chinline{added}

The command \doccommand{added} marks newly added text.
The new text is given in curly braces.

The optional argument contains key-value-pairs for author-id and comment.
The author-id has to be defined using \doccommand{definechangesauthor}.
If the comment contains special characters or spaces, use curly brackets to enclose the comment.

If a comment is given, the direct author markup at the changes text is omitted, because the author is printed in the comment.

\chexample{added}
\chresult{added}


\chnewcmd{deleted}

\chinline{deleted}

The command \doccommand{deleted} marks deleted text.
The deleted text is given in curly braces.

For the optional arguments see \doccommand{added} (\autoref{sec:ui:cmd:added}).

\chexample{deleted}
\chresult{deleted}



\chnewcmd{replaced}

\chinline{replaced}

The command \doccommand{replaced} marks replaced text.
The new and the replaced text are given in this order in curly braces.

For the optional arguments see \doccommand{added} (\autoref{sec:ui:cmd:added}).

The output of replaced text is a combination of added and deleted text, thus any change in their layout influences the layout of replaced text.

\chexample{replaced}
\chresult{replaced}



%^^A -- Highlighting and Comments ------------------------------------------------------
\subsection{Highlighting and Comments}
\label{sec:ui:comment}

\localtableofcontents

\chnewcmd{highlight}

\chinline{highlight}

The command \doccommand{highlight} highlights text.
The highlighted text is given in curly braces.

For the optional arguments see \doccommand{added} (\autoref{sec:ui:cmd:added}).

\chexample{highlight}
\chresult{highlight}


\chnewcmd{comment}

\chinline{comment}

The command \doccommand{comments} adds a comment to the document.
The comment is given in curly braces.

The command has only one optional argument: a key-value-pair for the author-id.
The author-id has to be defined using \doccommand{definechangesauthor}.

The comments are numbered automatically, the number is printed in the comment.

\chexample{comment}
\chresult{comment}




%^^A -- Overview of changes
\subsection{Overview of changes}
\label{sec:ui:overview}


\chnewcmd{listofchanges}

\chinline{listofchanges}

The command \doccommand{listofchanges} outputs a list or summary of changes.
The first \hologo{LaTeX}-run creates an auxiliary file, the second run uses the data of this file.
Therefore you need two \hologo{LaTeX}-runs for an up-to-date list of changes.

There are three optional arguments:

\begin{description}
	\item[\docoption{style}] list style
	\item[\docoption{title}] individual title
	\item[\docoption{show}] markup types
\end{description}

\paragraph{style}
The \docoption{style} argument defines the layout of the list of changes.
Three styles are defined:

\begin{description}
	\item[\docoption{list}] prints the list of changes like a list of figures (default)
	\item[\docoption{summary}] prints the number of changes grouped by author
	\item[\docoption{compactsummary}] same as \docoption{summary} but entries with count 0 are omitted
\end{description}

\paragraph{title}
The \docoption{title} argument is used to change the title for the list.
If you want to use special characters or spaces in the title, enclose it in curly braces.

\paragraph{show}
The \docoption{show} argument defines which types of change markup are shown in the list of changes.
You can combine the values using the \texttt{|} character.
For example if you want to show all additions and deletions, use \texttt{show=added|deleted}.

The following values are defined:

\begin{description}
	\item[\docoption{all}] show all types (default)
	\item[\docoption{added}] show only additions
	\item[\docoption{deleted}] show only deletions
	\item[\docoption{replaced}] show only replacements
	\item[\docoption{highlight}] show only highlights
	\item[\docoption{comment}] show only comments
\end{description}

\chexample{listofchanges}



%^^A -- Author management -----------------------------------------------------
\subsection{Author management}
\label{sec:ui:authormanagement}

\chnewcmd{definechangesauthor}

\chinline{definechangesauthor}

The command \doccommand{definechangesauthor} defines a new author for changes.
You have to define a unique author's id, special characters or spaces are not allowed within the author's id.

You may define a corresponding color and the author's name.
If you do not define a color, blue is used.

The author's name is used in the list of changes and in the markup if you set the corresponding option.

The package predefines one anonymous author without id.

\chexample{definechangesauthor}


%^^A -- Adaptation of the output -----------------------------------------------------
\subsection{Adaptation of the output}
\label{sec:ui:customizingoutput}

\localtableofcontents

\subsubsection{Values for markup command definitions}

If you want to adapt the markup output, you can use any \hologo{LaTeX}-commands and special values resp.\ macros of the \docpackage{changes} package.
Some values or macros are specific for each command, they are described in the corresponding sections.

The following values and macros can be used in each command:

\begin{itemize}
	\item any \hologo{LaTeX}-commands
	\item author's color can be used with color ``authorcolor''
	\item boolean test if colored change output is needed ``\doccommand{IfIsColored}''
\end{itemize}

I do not provide full access to all elements of the markup for every command in order to keep the macros simple.
For example, the author's id is only available for \doccommand{setcommentmarkup}.

The output of replaced text is a combination of added and deleted text.


\chnewcmd{setaddedmarkup}

\chinline{setaddedmarkup}

The command \doccommand{setaddedmarkup} defines the layout of added text.
The default markup is colored text, or the markup set with the option \docoption{markup} respectively \docoption{addedmarkup}.

Values for definition:

\begin{itemize}
	\item added text can be used with ``\#1''
\end{itemize}

The output of replaced text is a combination of added and deleted text, thus any change in their layout influences the layout of replaced text.

\chexample{setaddedmarkup}


\chnewcmd{setdeletedmarkup}

\chinline{setdeletedmarkup}

The command \doccommand{setdeletedmarkup} defines the layout of deleted text.
The default markup is striked-out, or the markup set with the option \docoption{markup} respectively \docoption{deletedmarkup}.

Values for definition:

\begin{itemize}
	\item deleted text can be used with ``\#1''
\end{itemize}

The output of replaced text is a combination of added and deleted text, thus any change in their layout influences the layout of replaced text.

\chexample{setdeletedmarkup}


\chnewcmd{sethighlightmarkup}

\chinline{sethighlightmarkup}

The command \doccommand{sethighlightmarkup} defines the layout of highlighted text.
The default markup is via a background color, or the markup set with the option \docoption{markup} respectively \docoption{highlightmarkup}.

Values for definition:

\begin{itemize}
	\item highlighted text can be used with ``\#1''
\end{itemize}

\chexample{sethighlightmarkup}


\chnewcmd{setcommentmarkup}

\chinline{setcommentmarkup}

The command \doccommand{setcommentmarkup} defines the layout of comments.
The default markup is a margin note, or the markup set with the option \docoption{markup} respectively \docoption{commentmarkup}.

Values for definition:

\begin{itemize}
	\item comment can be used with ``\#1''
	\item author's id can be used with ``\#2''
	\item author output (id or name) can be used with ``\#3''
	\item comment count of the autor can be used with counter ``authorcommentcount''
	\item boolean test if author is anonymous ``\doccommand{IfIsAnonymous}''
\end{itemize}

\chexample{setcommentmarkup}


\chnewcmd{setauthormarkup}

\chinline{setauthormarkup}

The command \doccommand{setauthormarkup} defines the layout of the author's markup in the text.
The default markup is a superscripted author's text.

Values for definition:

\begin{itemize}
	\item author output (id or name) can be used with ``\#1''
\end{itemize}

\chexample{setauthormarkup}


\chnewcmd{setauthormarkupposition}

\chinline{setauthormarkupposition}

The command \doccommand{setauthormarkupposition} defines the position of the author's markup relative to the changed text.
The default position is right of the changed text.

The following values for \emph{authormarkupposition} are defined:

\begin{description}
	\item [\docoption{right}] right of the text -- text\textsuperscript{author} (default)
	\item [\docoption{left}] left of the text -- \textsuperscript{author}text
\end{description}

\chexample{setauthormarkupposition}


\chnewcmd{setauthormarkuptext}

\chinline{setauthormarkuptext}

The command \doccommand{setauthormarkuptext} defines the text for the author's markup.
The default markup is the author's id.

The following values for \emph{authormarkuptext} are defined:

\begin{description}
	\item [\docoption{id}] author's id -- text\textsuperscript{id} (default)
	\item [\docoption{name}] author's name -- text\textsuperscript{authorname}
\end{description}

\chexample{setauthormarkuptext}


\chnewcmd{setanonymousname}

\chinline{setanonymousname}

The command \doccommand{setanonymousname} sets the anonymous author's name.
The default name is the language dependent equivalent of ``anonymous''.

This option is helpful if you are the only author and you want your name to be displayed at the changes.

\chexample{setanonymousname}


\chnewcmd{settruncatewidth}

\chinline{settruncatewidth}

The command \doccommand{settruncatewidth} sets the width of the truncation in the list of changes to the given width.
The default width is \texttt{0.6}\doccommand{textwidth}.

\chexample{settruncatewidth}



\chnewcmd{setsummarywidth}

\chinline{setsummarywidth}

The command \doccommand{setsummarywidth} sets the width of the list of changes in summary style to the given width.
The default width is \texttt{0.3}\doccommand{textwidth}.

\chexample{setsummarywidth}



\chnewcmd{setsummarytowidth}

\chinline{setsummarytowidth}

The command \doccommand{setsummarytowidth} sets the width of the list of changes in summary style to the width of the given text.

\chexample{setsummarytowidth}



\chnewcmd{setlocextension}

\chinline{setlocextension}

The command \doccommand{setlocextension} sets the extension of the auxiliary file for the list of changes (loc-file\footnote{%
	``loc'' stands for ``list of changes''.
}).
The default extension is ``\texttt{loc}''.

In the example, the loc-file for ``\texttt{foo.tex}'' would be named ``\texttt{foo.listofchanges}'' resp.\ ``\texttt{foo.lochg}'' instead of the default name ``\texttt{foo.loc}''.

\chexample{setlocextension}

\chimportant{Do not use a \hologo{LaTeX} standard file extension, such as ``toc'' or ``lof'', as this would collide with the normal \hologo{LaTeX} run.}



\chnewcmd{setsocextension}

\chinline{setsocextension}

The command \doccommand{setsocextension} sets the extension of the auxiliary file for the summary of changes (soc-file\footnote{%
	``soc'' stands for ``summary of changes''.
}).
The default extension is ``\texttt{soc}''.

In the example, the soc-file for ``\texttt{foo.tex}'' would be named ``\texttt{foo.changes}'' resp.\ ``\texttt{foo.chg}'' instead of the default name ``\texttt{foo.soc}''.

\chexample{setsocextension}

\chimportant{Do not use a \hologo{LaTeX} standard file extension, such as ``toc'' or ``lof'', as this would collide with the normal \hologo{LaTeX} run.}



%^^A -- packages
\subsection{Used packages}
\label{sec:ui:packages}

The \docpackage{changes}-package uses already existing packages for it's functions.
You will find detailed description of the packages in their distributions.

The following packages are always required and have to be installed for the \docpackage{changes}-package:
\begin{description}
	\item [etoolbox] provides an enhanced \doccommand{if}-commands, \emph{bools}, or list operations
	\item [truncate] truncation of texts (used for list of changes)
	\item [xkeyval] provides key-value-lists for parameters
	\item [xstring] improves string operations
\end{description}

The following packages are sometimes required and have to be installed if used by the corresponding option:
\begin{description}
	\item [todonotes] loaded if comments are layouted as todo notes (default markup)
	\item [ulem] loaded if text has to be striked, wavylined or exed out (default markup)
	\item [xcolor] loaded if colored text is used for markup (default markup)
\end{description}


%^^A ---- Remove markup from file
\cleardoublepage
\section{Remove markup from file}
\label{sec:remove-markup}

In order to remove the markup from the \hologo{LaTeX} files, you have to remove the commands by hand or use the script by Yvon Cui.
You find the script in the directory:

\chinline[, language=bash]{path_script}

The script removes all markups either keeping or rejecting the change.
You can select or deselect markup from removal using the interactive mode by starting the script without options.

The script requires \emph{python3}.

Use the script as follows:

\chinputlisting{, language=bash}{userdoc/script_pymergechanges}

Run the script with no options and files for a short help text:

\chinputlisting{, language=bash}{userdoc/script_pymergechanges_empty}

Known issues:

\begin{itemize}
	\item removes only markup that is used in one line, not markup that spans multiple lines
\end{itemize}


%^^A ---- Known problems and solutions
\cleardoublepage
\section{Known problems and solutions}
\label{sec:known-problems}

This section contains known problems and their solutions as far as I know some.
If your problem is not listed here, please see the issue tracker on gitlab if it contains your problem (a search exists):

\url{https://gitlab.com/ekleinod/changes/issues}

If your problem is not listed, please open a new issue for your problem.
Describe your problem as specific as possible, if possible, include a small example file with the problematic behavior.

\subsection{Special content}

Change markup of texts works well, it is possible to markup whole paragraphs.
You cannot markup:

\begin{itemize}
	\item figures
	\item tables
	\item headings
	\item some commands
	\item several paragraphs (sometimes)
\end{itemize}

You can try putting such text in an extra file and include in with \texttt{input}.
This works sometimes, give it a try.
Kudos to Charly Arenz for this tip.

\subsection{Footnotes and margin notes}

There is a problem of typesetting footnotes or margin notes in special environments, such as tables or the \emph{tabbing} environment.
Avoid this type of markup when using these environments.


\subsection{The \docpackage{ulem} package}

I am using the \docpackage{ulem} package for striking out text as default.
This causes problems with some commands and environments, e.g.

\begin{itemize}
	\item in math mode
	\item when using the \docpackage{siunitx} package
	\item when using the \doccommand{citet} or \doccommand{citep} command
\end{itemize}

In that case there are few good options, the best is to define the markup for deletions yourself and avoid the ulem package.
See

\begin{itemize}
	\item \autoref{sec:ui:options:deletedmarkup}
	\item \autoref{sec:ui:cmd:setdeletedmarkup}
\end{itemize}


\subsection{Command already defined}

Some packages use the same names for their commands as the \docpackage{changes} package, in particular \doccommand{comment} and \doccommand{highlight} are not originally named commands.

In this case, \docpackage{changes} may prefix its commands to avoid naming collisions.
This is controlled by the \docoption{commandnameprefix} option, see \autoref{sec:ui:options:commandnameprefix} for the documentation.

In order for this to work, the \docpackage{changes} package has to be loaded after the other packages or \docoption{commandnameprefix=always} must be selected.


%^^A -- Authors -------------------------------------------------------------
\cleardoublepage
\section{Authors}
\label{sec:authors}

Several authors contributed to the \docpackage{changes}-package.
Many bugs and problems were solved or their solution inspired via de.comp.text.tex.
Thanks.

The authors are (in alphabetical order):
\begin{itemize}
	\item Chiaradonna, Silvano
	\item Cui, Yvon
	\item Fischer, Ulrike
	\item Giovannini, Daniele
	\item Kleinod, Ekkart
	\item Le Garrec, Vincent
	\item Mittelbach, Frank
	\item Richardson, Alexander
	\item Voss, Herbert
	\item Wölfel, Philipp
	\item Wolter, Steve
\end{itemize}



%^^A -- Versions -------------------------------------------------------------
\cleardoublepage
\section{Versions}
\label{sec:versions}

For a list of versions and the changes within these version, please refer to

\url{https://gitlab.com/ekleinod/changes/blob/master/changelog.md}

Here you too find the implemented but not released changes for the new version.

If you are interested in planned new features, please see

\url{https://gitlab.com/ekleinod/changes/milestones}


%^^A ---- copyright, license
\cleardoublepage
\section{Distribution, Copyright, License}

Copyright 2007-2021 Ekkart Kleinod (\href{mailto:ekleinod@edgesoft.de}{ekleinod@edgesoft.de})

This work may be distributed and/or modified under the conditions of the \hologo{LaTeX} Project Public License, either version~1.3 of this license or any later version.
The latest version of this license is in \url{http://www.latex-project.org/lppl.txt} and version~1.3 or later is part of all distributions of \hologo{LaTeX} version 2005/12/01 or later.

This work has the LPPL maintenance status ``maintained''.
The current maintainer of this work is Ekkart Kleinod.

This work consists of the files

\begin{tabbing}
	mm\=\kill
	\>\texttt{source/latex/changes/changes.drv}\\
	\>\texttt{source/latex/changes/changes.dtx}\\
	\>\texttt{source/latex/changes/changes.ins}\\
	\>\texttt{source/latex/changes/examples.dtx}\\
	\>\texttt{source/latex/changes/regression.dtx}\\
	\>\texttt{source/latex/changes/README}\\
	\>\texttt{source/latex/changes/userdoc/*.tex}\\

	\>\texttt{scripts/changes/pyMergeChanges.py}
\end{tabbing}


and the derived files

\begin{tabbing}
	mm\=\kill
	\>\texttt{doc/latex/changes/changes.english.pdf}\\
	\>\texttt{doc/latex/changes/changes.english.withcode.pdf}\\
	\>\texttt{doc/latex/changes/changes.ngerman.pdf}\\

	\>\texttt{doc/latex/changes/examples/changes.example.*.tex}\\
	\>\texttt{doc/latex/changes/examples/changes.example.*.pdf}\\

	\>\texttt{doc/latex/changes/regression/changes.regression.*.tex}\\
	\>\texttt{doc/latex/changes/regression/changes.regression.*.pdf}\\

	\>\texttt{tex/latex/changes/changes.sty}
\end{tabbing}


%^^A end of user documentation
