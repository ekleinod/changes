The changes-package allows the user to manually markup changes of text, such as additions, deletions, or replacements.
Changed text is shown in a different color; deleted text is striked out.
Additionally, text can be highlighted and/or commented.
The package allows free definition of additional authors and their associated color.
It also allows you to change the markup of changes, authors, highlights or comments.

Release 4.0.0 is a substantial rewrite with some additions, changes and bugfixes.
Release 4.0.3 is a bugfix release for a compile error with cite command with final option.

For a list of changes see: https://gitlab.com/ekleinod/changes/-/blob/main/changelog.md

Please inform me about any errors or improvements as you see fit.
